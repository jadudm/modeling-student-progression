#lang racket

(require "ga.rkt")

(courses-per-year 14)
(new-stu-per-year 250)
(courses-required 6)
(max-courses-per-term 2)
(mutation-percentage 15)

(keep-from-each-generation 15)
(number-of-dishes 60)
(generations 20)

(run-generations (generations)
                 (make-dishes (number-of-dishes) (hash-keys codons)))
